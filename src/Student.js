// @flow

class Student {
  name: string;

  age: number;

  
  gender: string;

  constructor(name: string, age: number, gender: string) {
    this.name = name;
    this.age = age;
    this.gender = gender;
  }

  sayHello() {
    return `Labas, labas! As esu ${this.name}`;
  }

  changeName(name: string) {
    this.name = name;
  }
}

export default Student;
