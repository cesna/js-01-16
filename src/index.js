// @flow

// import Student from './Student';

// const studentJonas = new Student('Jonas', 25);
// const studentAlbinas = new Student('Albinas', 30, 'male');
// const studentPetras = new Student('Petras', 55, 'male');
// const studentAiste = new Student('Aiste', 25, 'female');
// const studentAugustė = new Student('Augustė', 27, 'female');

/* eslint-disable no-console */
// console.log(studentJonas.sayHello());

// const ages = [222, 512, 12, 22, 25, 11, 2, 95];
// const students = [
//   studentJonas,
//   studentAlbinas,
//   studentPetras,
//   studentAiste,
//   studentAugustė,
// ];

// const olderThan20 = ages.filter(age => age > 20);

// console.log(ages);
// console.log(olderThan20);
// console.table(students);

// const maleStudents = students.filter(
//   student => student.gender !== undefined && student.gender !== 'female',
// );
// const femaleStudents = students.filter(
//   student => student.gender !== undefined && student.gender !== 'male' && student.age >= 27,
// );

// console.table(maleStudents);
// console.table(femaleStudents);

// const person = {
//   name: 'Julius',
//   surname: 'Burbulis',
//   school: 'BTA',
//   skills: ['Java', 'Android', 'iOS', 'ES6'],
//   links: {
//     social: {
//       twitter: 'http://twitter.com/burjulius',
//       facebook: 'http://facebook.com',
//     },
//   },
// };

// const firsName = person.name;
// const schoolName = person.school;
// const lastName = person.surname;

// const twitter = person.links.social.twitter;
// const facebook = person.links.social.facebook;

// const {
//   twitter = 'twitterio nuoroda',
//   facebook = 'facebooko nuoroda',
//   youtube = 'youtubo linkas',
// } = person.links.social;

// console.log(`Vardas: ${firsName}; Pavarde: ${lastName}. Mokykla: ${schoolName}`);
// console.log(`Vardas: ${name}; Pavarde: ${surname}. Mokykla: ${school}`);
// console.log(`Twitter: ${twitter}; Facebook: ${facebook}; Youtube: ${youtube}`);
