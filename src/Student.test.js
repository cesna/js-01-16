import Student from './Student';

describe('Student', () => {
  describe('sayHello', () => {
    test('should return Labas, labas! As esu Jonas', () => {
      const studentJonas = new Student('Jonas');
      expect(studentJonas.sayHello()).toBe('Labas, labas! As esu Jonas');
    });

    test('should return the same name with utf-8 encoding', () => {
      const studentAuguste = new Student('Augustė');
      expect(studentAuguste.sayHello()).toBe('Labas, labas! As esu Augustė');
    });
  });
});
